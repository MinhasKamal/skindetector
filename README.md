# Skin Detector
#### Detects Human Skin From Image
[This program](http://minhaskamal.github.io/SkinDetector) is a very simple machine learning implementation. Only by altering training data it can detect any type of region based on pixel.

### How to Run?
1. Integrate [Egami](https://github.com/MinhasKamal/Egami) with the project. Egami is used only for reading and writing image.
2. For training the sytem run **SkinDetectorTrainer.java**.
3. Then run **SkinDetectorTester.java** or **SkinDetectorTester2.java** for getting output (change file-paths in the main method according to the need).

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>SkinDetector is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
